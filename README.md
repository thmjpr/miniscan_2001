# miniScan_2001

Miniature scanner card for DMM6500

![2020-03-15_11.12.57](/uploads/e76f31faad5386281b825740014eb04f/2020-03-15_11.12.57.jpg)

![2021-09-19_15.03.34](/uploads/f287a51052364dd78371fb4783801e12/2021-09-19_15.03.34.jpg)

## Features
- 5 sense and 5 input channels
- Usable as 10 input channels by fitting two hardware jumpers
- 350V rated photoMOS inputs, 200V GSP overvoltage protection
- Input resistance ~30 Ohms (LH1520)

## Hardware

- Bluepill board with STM32F103C8T6
- PhotoMOS switches (LH1520 or equivalent)


## PCB
- Rev_1: Connector pinout backwards! Won't work.
- Rev_2: Working revision.


## Issues
- 

## Credit

ALL credit goes to cozdas, incredible documentation here: https://github.com/cozdas/CozScan2020


## License

MIT license where applicable.  
