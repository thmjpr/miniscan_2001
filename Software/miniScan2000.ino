//===================
// CozScan2020
//===================

/* *****
BSD 3-Clause License

Copyright (c) 2019, Cuneyt Ozdas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***** */


// Notes:
// ==========
// using interrupts was too slow, the clock ISR was
// totally missing the data. Actively waiting the rising clock
// edge is much faster, now the data is properly sampled before the falling edge
// of the clock


//Bluepill pins

//namespace IO
enum outputs
{
	CH1 = PA0,
	CH2 = PA1,
	CH3 = PA2,
	CH4 = PA3,
	CH5 = PA4,
	CH11 = PA5,
	CH12 = PA6,
	CH13 = PA7,
	CH14 = PA8,
	CH15 = PA9,
	LED = PC13,
}
;

constexpr std::initializer_list<outputs> all_outputs = { CH1, CH2, CH3, CH4, CH5, CH11, CH12, CH13, CH14, CH15, LED };
constexpr std::initializer_list<outputs> relay_outputs = { CH1, CH2, CH3, CH4, CH5, CH11, CH12, CH13, CH14, CH15 };

enum inputs
{
	Data   = PB6,
	Clock  = PB7,
	Strobe = PB8,
	Reset  = PB9,
};

constexpr std::initializer_list<inputs> all_inputs = { Data, Clock, Strobe, Reset };

constexpr bool Debug = false;

/* SCAN CHANNEL TO PIN MAPPING
chan DPin   NAME  PORT    BIT   MicroPin
0     --, 
1     15    PA0   
2     14    PA1   
3     16    PA2   
4     17    PA3   
5     1     PA4   
6     0     P   
7     2     P  
8     3     P  
9     4     P   
10    5     P   

11    21    PA5   
12    22    PA6   
13    23    PA7   
14    12    PA8   
15    11    PA9   
16    10    P   
17    9     P   
18    8     P   
19    7     P   
20    6     P   

21    13    P   
22    30    P   
*/

/* CONTROL BITS TO CHANNEL AND PIN MAPPINGS
//first sent bit is MSB (bit48), last sent bit is LSB (bit0)
//even bits: reset coil
//odd bits: set coil
//ctrl bit, chan, Dpin, PORTBIT,  PORT,   BIT
0           11    21    P
2           12    22    P
4           13    23    P
6           14    12    P
8           15    11    P
10          16    10    P
12          17    9     P
14          18    8     P
16          19    7     P
18          20    6     P
20          1     15    P
22          2     14    P
24          3     16    P
26          4     17    P
28          5     1     P
30          6     0     P
32          7     2     P
34          8     3     P
36          9     4     P
38          10    5     P
40          21    13    P
*/

//temp variables to hold register set/clear bits
uint8_t _PB, _PC, _PD, _PE, _PF;

//If using sense ports:
//If not using sense ports:

//channel number to pin mapping. Note the scanner card starts from chan1 not 0
uint32_t channel_to_pin[] = { CH11, CH12, CH13, CH14, CH15, 0, 0, 0, 0, 0, CH1, CH2, CH3, CH4, CH5, 0, 0, 0, 0, 0, 0 };
constexpr std::initializer_list<outputs> channel_to_pin_map = { CH1, CH2, CH3, CH4, CH5, CH11, CH12, CH13, CH14, CH15 };

//control bits are shifted into this 64bit variable
uint64_t  state = 0;
uint8_t   numBitsCollected = 0;


void TestRun()
{
	Serial.print("\nStart test run. ");
	delay(5000);

	for (;;)
	{
		//reset chan 21, this will connect bus_b to input
		state = 1ll << 40;
		numBitsCollected = 48;
		strobe();

		//connect all channels to input one by one
		for(int i = 0 ; i < 20 ; ++i)
		{
			//close relay
			state = 1ll << (2*i + 1);
			numBitsCollected = 48;
			strobe();
			Serial.print(" Output enabled #: "); Serial.print(i, DEC);
			delay(5000);

			//open relay
			state = 1ll << (2*i);
			numBitsCollected = 48;
			strobe();
			delay(50);
			led(!led());
		}
	}
}

//return true if internal relay states makes sense
bool SanityCheck(uint32_t busA, uint32_t busB)
{
	//check if busses have more than 1 bit set  
	bool tooManyBitsInA = busA & (busA - 1);  //this op clears the rightmost 1 bit. if result is nonzero, then more than one bit was set 
	bool tooManyBitsInB = busB & (busB - 1);  //this op clears the rightmost 1 bit. if result is nonzero, then more than one bit was set 

	if(tooManyBitsInA | tooManyBitsInB)
	  return false;
    
	bool busBtoSense  = 0;     //Bus B not used 
	bool busBtoIn     = 0;     //Bus B not used 

	//bus B should be connected to either In or sense
	if(busBtoSense && busBtoIn)
	  return false;

	//now make sure that if both busA and busB has connected channels, then the busB should not connect to input (4-wire should be on)
	if(busA && busB && !busBtoSense)
	  return false;

	//passed all checks
	return true;
}

//we process the bits in two passes to guarantee the "break before connect" ordering.
//pass1: read the odd bits, which are reset coil commands, and clear the corresponding port bits for those channels
//pass2: read the even bits, which are set coil commands, and set the corresponding port bits for those channels
//(heavy bit manipulation for fastest write)
void strobe()
{
	//we can safely ignore the coil de-energizing events
	if(state == 0)
	  return;

	//we are out of sync if strobe comes not after exactly 48 bits. 
	//Disconnect everything to be on the safer side
	/*
	if(numBitsCollected!=48)
	{
	return;
	}
	*/

	//Disconnect all relays
	for(auto o : relay_outputs)
	{
		pinMode(o, OUTPUT);
		digitalWrite(o, LOW);
	}

	//Pass0: process the reset bits (even bits)
	//necessary? everything is already shut off
	{ 
		uint64_t st = state;
		for (int i = 0; i < 20; i++, st >>= 2)
		{
			if((st & 1) & (channel_to_pin[i] != 0))  //if set, channel active, and channel is mapped
			{
				digitalWrite(channel_to_pin[i], LOW);
			}
		}

		//also if the SENSE connection is set, break the INPUT connection now
		//if(state&(1ll<<41))
		//  PORTD &= ~(1<<5);
	}

	//Pass1:  process the set bits (odd bits)
	{
		uint64_t st = state >> 1;
		for (int i = 0; i < 20; i++, st >>= 2)
		{
			if ((st & 1) & (channel_to_pin[i] != 0))  //if set, channel active, and channel is mapped
				{
					digitalWrite(channel_to_pin[i], HIGH);
				}
		}

		//BusB2In = !BusB2Sense (PD5 = !PC7)
		//_PD = (PD&~(1<<5)) | (((~_PC)&(1<<7))>>2);
		/*
		if(0) //_PC&(1<<7)) BUS B not used
		  _PD &= ~(1<<5);
		else 
		  _PD |= (1<<5);*/

		//arbitrarily arranged bitmap of each bus for bit counting (busA: chan 1..10, busB: chan 11..20)
		uint32_t busA = (_PB & 0x0Fl) | ((_PC & 0x40l) << 8) | ((_PD & 0x1Fl) << 16);                             //chans 1..10
		uint32_t busB = (_PB & 0xF0l) | ((_PD & 0xC0l) << 8)  | ((_PE & 0x40l) << 16) | ((_PF & 0x13l) << 24);    //chans 11..20

		if(SanityCheck(busA, busB))
		{   
			//PORTB |= _PB;
			//PORTE |= _PE;
			//PORTF |= _PF;

			//if no channel is active on BusB, then let BusB dangle (reduces leakage between BusA and BusB channels)

			//busB not used for miniScan, have to manual jumper if you want to use sense lines.
			if(busB)
			{
				//PORTC |= _PC;
				//PORTD |= _PD;
			}
			else
			{
				//PORTC |= (_PC & ~(1<<7));
				//PORTD |= (_PD & ~(1<<5));       
			}
		}
	}
  
	state = 0;  
	numBitsCollected = 0;
}

void setup() 
{
	led(true);

	if (Debug)
	{
		Serial.begin(115200);
		while (!Serial)    //USB serial debugging only
			{}
  
		Serial.print("Starting ");
	}
  

	//set all channel pins to output
	for(auto o : all_outputs)
	{
		pinMode(o, OUTPUT);
		digitalWrite(o, LOW);
	}

	//set the input pins
	pinMode(inputs::Data, INPUT);
	pinMode(inputs::Clock, INPUT);
	pinMode(inputs::Strobe, INPUT);

	//port states
	_PB = _PC = _PD = _PE = _PF = 0;

	state = 0;  
	numBitsCollected = 0;

	while (Debug)
	{
		TestRun();
		delay(1000);
	}
}

//active waiting the pin states is much faster than interrupt driven system
//busy waiting the clocks and strobe there in a very thight loop
void loop() 
{
	//cli(); //disabling interrupts right away causes the Arduino IDE fail to upload the sketch.

	while(1)
	{
		//wait rising edge of the clock
		while(!(digitalRead(inputs::Clock)))
		{
			//while waiting check strobe rising edge
			if(digitalRead(inputs::Strobe)) 
			{
				strobe();
				//don't need to wait strobe signal's falling edge, even if strobe retriggers, since state will be 0, it'll return immediately 
			}
		}

		//we delay disabling interrupts because if this executes right away the 
		//uploader can't upload (probably verify) the sketch thus giving error during upload.
		//I'm guessing that the sketch runs before the verify begins and when interrupts are disabled
		//USB doesn't work.
		//cli();  
    
		//sample
		uint8_t b = (digitalRead(inputs::Data)  &  1);
		state = (state << 1) | b;
		numBitsCollected++;
    
		//wait falling clock edge
		while(digitalRead(inputs::Clock));
	}
}

//Write LED
void led(bool state)
{
	if (state)
		digitalWrite(outputs::LED, HIGH);
	else
		digitalWrite(outputs::LED, LOW);
    
}

//Read LED state
bool led(void)
{
	return digitalRead(outputs::LED);
}

//void debugPrint()
